﻿using System;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Media;
using System.IO;
using Microsoft.Win32;

namespace lfwb_hrm
{
    public partial class Form1 : Form
    {
        bool Working;
        int SleepCounter;
        bool LastState;
        Thread T;
        public Form1()
        {
            InitializeComponent();
            Working = true;
            LastState = false;
            SleepCounter = 0;
            T = new Thread(Monitoring) { IsBackground = true };
            T.Start();
            autorunToolStripMenuItem.Checked = Properties.Settings.Default.autorun;
            soundToolStripMenuItem.Checked = Properties.Settings.Default.sound;
            notificationsToolStripMenuItem.Checked = Properties.Settings.Default.notifications;

            if(Properties.Settings.Default.ServerId == 1)
            {
                s1ToolStripMenuItem.Checked = true;
            }
            else if (Properties.Settings.Default.ServerId == 2)
            {
                s2ToolStripMenuItem.Checked = true;
            }
            else if (Properties.Settings.Default.ServerId == 3)
            {
                s3ToolStripMenuItem.Checked = true;
            }
            else if (Properties.Settings.Default.ServerId == 4)
            {
                backupToolStripMenuItem.Checked = true;
            }
            else if (Properties.Settings.Default.ServerId == 5)
            {
                russianToolStripMenuItem.Checked = true;
            }

            if (Properties.Settings.Default.firstrun)
            {
                //autorunToolStripMenuItem_Click(null, null);
                Properties.Settings.Default.firstrun = false;
                Properties.Settings.Default.Save();
            }
        }

        private void Monitoring()
        {
            TcpClient TC;
            while(Working)
            {
                SleepCounter = 0;
                try
                {
                    TC = new TcpClient();
                    TC.NoDelay = true;
                    TC.ReceiveTimeout = 1000;
                    TC.SendTimeout = 1000;
                    TC.Connect(Properties.Settings.Default.ip, Properties.Settings.Default.port);
                    Thread.Sleep(500);
                    TC.Close();
                    Alive();
                }
                catch
                {
                    Dead();
                }
                while(SleepCounter < 10)
                {
                    Thread.Sleep(550);
                    SleepCounter++;
                }
            }
        }

        private void Alive()
        {
            if (LastState != true)
            {
                LastState = true;
                notifyIcon1.Icon = Properties.Resources.red_heart;
                if (Properties.Settings.Default.notifications)
                {
                    notifyIcon1.ShowBalloonTip(400,"It's up!","Yeah!",ToolTipIcon.Info);
                }
                if (Properties.Settings.Default.sound)
                {
                    (new SoundPlayer(Properties.Resources.dark)).Play();
                }
            }
        }
        private void Dead()
        {
            if(LastState != false)
            {
                LastState = false;
                notifyIcon1.Icon = Properties.Resources.black_heart;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            Working = false;
            Close();
            
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Working = false;
            Close();
        }

        private void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("byond://" + Properties.Settings.Default.ip + ":" + Properties.Settings.Default.port.ToString());
        }

        private void s1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            s1ToolStripMenuItem.Checked = true;
            s2ToolStripMenuItem.Checked = false;
            s3ToolStripMenuItem.Checked = false;
            backupToolStripMenuItem.Checked = false;
            russianToolStripMenuItem.Checked = false;
            Properties.Settings.Default.ip = "play.lfwb.ru";
            Properties.Settings.Default.port = 1923;
            Properties.Settings.Default.ServerId = 1;
            Properties.Settings.Default.Save();
            SleepCounter = 11;
        }
        
        private void s2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            s1ToolStripMenuItem.Checked = false;
            s2ToolStripMenuItem.Checked = true;
            s3ToolStripMenuItem.Checked = false;
            backupToolStripMenuItem.Checked = false;
            russianToolStripMenuItem.Checked = false;
            Properties.Settings.Default.ip = "play.lfwb.ru";
            Properties.Settings.Default.port = 2422;
            Properties.Settings.Default.ServerId = 2;
            Properties.Settings.Default.Save();
            SleepCounter = 11;
        }

        private void s3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            s1ToolStripMenuItem.Checked = false;
            s2ToolStripMenuItem.Checked = false;
            s3ToolStripMenuItem.Checked = true;
            backupToolStripMenuItem.Checked = false;
            russianToolStripMenuItem.Checked = false;
            Properties.Settings.Default.ip = "play.lfwb.ru";
            Properties.Settings.Default.port = 1776;
            Properties.Settings.Default.ServerId = 3;
            Properties.Settings.Default.Save();
            SleepCounter = 11;
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            s1ToolStripMenuItem.Checked = false;
            s2ToolStripMenuItem.Checked = false;
            s3ToolStripMenuItem.Checked = false;
            backupToolStripMenuItem.Checked = true;
            russianToolStripMenuItem.Checked = false;
            Properties.Settings.Default.ip = "play.lfwb.ru";
            Properties.Settings.Default.port = 1945;
            Properties.Settings.Default.ServerId = 4;
            Properties.Settings.Default.Save();
            SleepCounter = 11;
        }

        private void russianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            s1ToolStripMenuItem.Checked = false;
            s2ToolStripMenuItem.Checked = false;
            s3ToolStripMenuItem.Checked = false;
            backupToolStripMenuItem.Checked = false;
            russianToolStripMenuItem.Checked = true;
            Properties.Settings.Default.ip = "play.lfwb.ru";
            Properties.Settings.Default.port = 1984;
            Properties.Settings.Default.ServerId = 5;
            Properties.Settings.Default.Save();
            SleepCounter = 11;
        }

        private void autorunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.autorun = !Properties.Settings.Default.autorun;
            autorunToolStripMenuItem.Checked = Properties.Settings.Default.autorun;
            Properties.Settings.Default.Save();
            RegistryKey AutorunRegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (Properties.Settings.Default.autorun)
            {
                string CurrentFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string AppDataFilePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Lifeweb_Monitoring.exe";
                if (CurrentFilePath != AppDataFilePath)
                {
                    File.Copy(CurrentFilePath, AppDataFilePath, true);
                }
                AutorunRegistryKey.SetValue("LifewebMonitoring", AppDataFilePath);
            }
            else
            {
                AutorunRegistryKey.DeleteValue("LifewebMonitoring", false);
            }
        }

        private void notificationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.notifications = !Properties.Settings.Default.notifications;
            notificationsToolStripMenuItem.Checked = Properties.Settings.Default.notifications;
            Properties.Settings.Default.Save();
        }

        private void soundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.sound = !Properties.Settings.Default.sound;
            soundToolStripMenuItem.Checked = Properties.Settings.Default.sound;
            Properties.Settings.Default.Save();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BeginInvoke(new MethodInvoker(delegate
            {
                Hide();
            }));
        }
    }
}
